#include <toppra/algorithm/toppra.hpp>
#include <toppra/constraint/linear_joint_acceleration.hpp>
#include <toppra/constraint/linear_joint_velocity.hpp>
#include <toppra/geometric_path/piecewise_poly_path.hpp>
#include <toppra/parametrizer/const_accel.hpp>
#include <toppra/toppra.hpp>

#include <chrono>

void formatVecToMat(
    const std::vector<Eigen::VectorXd,
                      Eigen::aligned_allocator<Eigen::VectorXd>> &vec,
    Eigen::MatrixXd &mat) {
  mat.resize(vec.at(0).rows(), vec.size());
  for (size_t i = 0; i < vec.size(); i++) {
    mat.col(i) = vec.at(i);
  }
}

toppra::BoundaryCond
makeBoundaryCond(const int order,
                 const std::vector<toppra::value_type> &values) {
  toppra::BoundaryCond cond;
  cond.order = order;
  cond.values.resize(values.size());
  for (std::size_t i = 0; i < values.size(); i++)
    cond.values(i) = values[i];
  return cond;
}

int main(int argc, char *argv[]) {
  const bool printInfo = false;

  auto t1 = std::chrono::high_resolution_clock::now();

  //#### create piecewise polynomial geometric path ####
  const int numJoints = 6;

  toppra::Vector position0{numJoints}, position1{numJoints},
      position2{numJoints};
  position0 << 1, 6, 0, 3, 10, 5;
  position1 << 5, 3, 8, 2, 0, 0;
  position2 << 1, 6, 9, 17, 6, 4;
  toppra::Vectors positions = {position0, position1, position2};
  ;

  toppra::Vector times(3);
  times << 0, 1, 2;

  toppra::BoundaryCond bc = makeBoundaryCond(2, {0, 0, 0, 0, 0, 0});
  std::array<toppra::BoundaryCond, 2> bc_type{bc, bc};

  toppra::GeometricPathPtr path;
  path = std::make_shared<toppra::PiecewisePolyPath>(positions, times, bc_type);

  //#### create linear joint-space constraints ####
  toppra::Vector velLimitLower = Eigen::VectorXd::Constant(numJoints, -1);
  toppra::Vector velLimitUpper = Eigen::VectorXd::Constant(numJoints, 1);
  toppra::Vector accLimitLower = Eigen::VectorXd::Constant(numJoints, -1);
  toppra::Vector accLimitUpper = Eigen::VectorXd::Constant(numJoints, 1);

  toppra::LinearConstraintPtr ljv, lja;
  ljv = std::make_shared<toppra::constraint::LinearJointVelocity>(
      velLimitLower, velLimitUpper);
  lja = std::make_shared<toppra::constraint::LinearJointAcceleration>(
      accLimitLower, accLimitUpper);
  lja->discretizationType(toppra::DiscretizationType::Interpolation);
  toppra::LinearConstraintPtrs constraints{ljv, lja};

  //#### create toppra ####
  toppra::PathParametrizationAlgorithmPtr algo =
      std::make_shared<toppra::algorithm::TOPPRA>(constraints, path);
  toppra::ReturnCode rc1 = algo->computePathParametrization(0, 0);
  if (printInfo)
    std::cout << "rc1 = " << int(rc1) << std::endl;
  // toppra::ReturnCode::OK);

  toppra::ParametrizationData pd = algo->getParameterizationData();
  if (printInfo)
    std::cout << "pd.gridpoints \n " << pd.gridpoints.transpose() << std::endl;
  if (printInfo)
    std::cout << "pd.parametrization \n " << pd.parametrization.transpose()
              << std::endl;
  if (printInfo)
    std::cout << "pd.controllable_sets \n " << pd.controllable_sets.transpose()
              << std::endl;
  if (printInfo)
    std::cout << "pd.feasible_sets \n " << pd.feasible_sets.transpose()
              << std::endl;
  if (printInfo)
    std::cout << "pd.ret_code = " << int(pd.ret_code) << std::endl;

  //#### create constant accelaration parametrizer ####
  toppra::Vector gridpoints =
      pd.gridpoints; // Grid-points used for solving the discretized problem.
  toppra::Vector vsquared =
      pd.parametrization; // Output parametrization (squared path velocity)
  std::shared_ptr<toppra::parametrizer::ConstAccel> ca =
      std::make_shared<toppra::parametrizer::ConstAccel>(path, gridpoints,
                                                         vsquared);
  if (printInfo)
    std::cout << "ca->validate() = " << ca->validate() << std::endl;

  Eigen::Matrix<toppra::value_type, 1, 2> interval2;
  interval2 = ca->pathInterval();
  if (printInfo)
    std::cout << "interval2 = " << interval2 << std::endl;

  double time_step = 0.1;
  int length2 = (interval2(1) - interval2(0)) / time_step;
  if (printInfo)
    std::cout << "length2 = " << length2 << std::endl;
  toppra::Vector times2 =
      toppra::Vector::LinSpaced(length2, interval2(0), interval2(1));
  toppra::Vectors path_pos2;
  path_pos2 = ca->eval(times2, 0); // TODO this function call fails
  toppra::Vectors path_vel2;
  path_vel2 = ca->eval(times2, 1); // TODO this function call fails
  toppra::Vectors path_acc2;
  path_acc2 = ca->eval(times2, 2); // TODO this function call fails

  auto t2 = std::chrono::high_resolution_clock::now();
  auto duration =
      std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

  std::cout << "duration (us) = " << duration << std::endl;

  Eigen::MatrixXd path_pos2_ = Eigen::MatrixXd::Zero(numJoints, length2);
  Eigen::MatrixXd path_vel2_ = Eigen::MatrixXd::Zero(numJoints, length2);
  Eigen::MatrixXd path_acc2_ = Eigen::MatrixXd::Zero(numJoints, length2);
  formatVecToMat(path_pos2, path_pos2_);
  formatVecToMat(path_vel2, path_vel2_);
  formatVecToMat(path_acc2, path_acc2_);
  if (printInfo)
    std::cout << "path_pos2_\n " << path_pos2_.transpose() << std::endl;
  if (printInfo)
    std::cout << "path_vel2_\n " << path_vel2_.transpose() << std::endl;
  if (printInfo)
    std::cout << "path_acc2_\n " << path_acc2_.transpose() << std::endl;
  if (printInfo)
    std::cout << "times2 \n " << times2 << std::endl;
}
